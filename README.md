# pre-proposal

The pre-proposal for the RMC should gives the reader an overview of the planned
work within 300 words. The proposal is hosted [on
bookdown](https://bookdown.org/sibbe_l_bakker/pre-proposal/pre-proposal.html).

# Assignment

Write the pre-proposal of approximately 300 words. Try to organise your
thoughts and ideas in a structured and concise manner utilizing what you
learned in the lecture and applying the form of the Nature Abstract. The
pre-proposal should attract the attention and interest of the reader. Be
concise and direct, use a clear structure to build up your proposal in a
cohesive manner. Optimally you should not exceed the limit of 300 words, but
you may extend it to 400. There is no minimum limit. Do realise that if you can
get the message across in less words, even better. Finding the balance between
conciseness and comprehensiveness is the challenge!  The pre-proposal should
include the following components:

* [x] Title
* [x] Keywords
* [x] Motivation: relevance and background of the research topic
* [x] Problem statement: urgency of problem, main objective of the research
  project
* [x] State of the art and knowledge gap: Why is this research required?
* [x] Objectives/hypotheses: What is your focus?
* [x] Approach: methodology how do you aim to conduct the research?
* [x] Main anticipated results and expected outcomes
* [x] Possible conclusions and/or implications
* [x] Three to five literature references
